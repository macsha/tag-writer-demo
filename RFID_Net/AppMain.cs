﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace RFID
{

    public partial class AppMain : Form
    {
        //当前读写器句柄，用于所有读写器操作的参数
        protected IntPtr _handle = IntPtr.Zero;
        /// <summary>
        /// 验证句柄的值是否有效
        /// </summary>
        private bool InvalidHandle
        {
            get
            {
                if (_handle == IntPtr.Zero || -1 == (int)_handle)
                {
                    return false;
                }
                return true;
            }
        }
        public AppMain()
        {
            InitializeComponent();
            this.cbCom.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //定义一个当前句柄，除OpenComm外所有被调用的方法都将需要此句柄。
            IntPtr p = IntPtr.Zero;
            try
            {
                int portNum = 1;//串口号
                p = EPCSDKHelper.OpenComm(portNum);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            //定义需要接受的参数，
            int main = 0;
            int sub = 0;
            //out指此参数为输出参数
            bool b = EPCSDKHelper.ReadFirmwareVersion(p, out main, out sub,0);


            int paramNum = 4;
            byte[] bo = new byte[paramNum];
            //vc中byte*类型对应的C#类型为byte[],需要在调用方法前，先实例参数。如果byte长度不确认的话，就把长度设置大点。
            EPCSDKHelper.GetReaderParameters(p, 0, 4, bo,0);

            //其他功能类似
        }

        /// <summary>
        /// 打开读写器
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOpen_Click(object sender, EventArgs e)
        {
            int portNum = cbCom.SelectedIndex + 1;//串口号

            if (!InvalidHandle)
            {
                //打开读写器，成功后会返回非零句柄
                _handle = EPCSDKHelper.OpenComm(portNum);
                if (InvalidHandle)
                {
                    toolStripStatusLabel1.Text = string.Format("SUCCESS!", portNum);
                    if (EPCSDKHelper.StopReading(_handle, 0))
                        toolStripStatusLabel1.Text = string.Format("Stop SUCCESS!");
                    else
                        toolStripStatusLabel1.Text = string.Format("Stop FAILED");
                }
                else
                {
                    toolStripStatusLabel1.Text = string.Format("FAILED！", portNum);
                }
            }
            else
            {
                toolStripStatusLabel1.Text = string.Format("ON！", portNum);
            }
        }
        /// <summary>
        /// 关闭读写器
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            int portNum = cbCom.SelectedIndex + 1;//串口号
            if (InvalidHandle)
            {
                //关闭读写器，使用打开读写器时返回的句柄作为参数
                EPCSDKHelper.CloseComm(_handle);
                _handle = IntPtr.Zero;
                toolStripStatusLabel1.Text = string.Format("Close the serial port {0} success!", portNum);
            }
            else
            {
                toolStripStatusLabel1.Text = string.Format("Serial port {0} is not open!", portNum);
            }
        }

        /// <summary>
        /// 读取Firmware版本
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFirmwareVer_Click(object sender, EventArgs e)
        {
            int mainVersion = 0;
            int subVersion = 0;
            if (EPCSDKHelper.ReadFirmwareVersion(_handle, out mainVersion, out subVersion, 0))
            {
                toolStripStatusLabel1.Text = string.Format("Firmware version: {0}.{1}", mainVersion, subVersion);
            }
            else
            {
                toolStripStatusLabel1.Text = string.Format("Failed to read Firmware version!");
            }
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            if (EPCSDKHelper.StopReading(_handle, 0))
                toolStripStatusLabel1.Text = string.Format("Stop reading tag success!");
            else
                toolStripStatusLabel1.Text = string.Format("Stop reading tag failed!");
        }

        private void btnResumeReading_Click(object sender, EventArgs e)
        {
            if (EPCSDKHelper.ResumeReading(_handle, 0))
                toolStripStatusLabel1.Text = string.Format("Reset the reading head successfully!");
            else
                toolStripStatusLabel1.Text = string.Format("Reset readhead failed!");
        }

        /// <summary>
        /// 读取单卡处理事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timerReadSingleTag_Tick(object sender, EventArgs e)
        {
            byte[] id = new byte[12];    // Tag id length is 12
            byte[] devNo=new byte[1]; 
            byte[] antennaNo = new byte[1]; 
            if (EPCSDKHelper.IdentifyUploadedSingleTag(_handle, id, devNo, antennaNo))
            {
                this.rtbTag.Text = string.Format("{0}    Device No:{1}  Antenna No: {2}", TextEncoder.ByteArrayToHexString(id), devNo[0], antennaNo[0]+1);
            }
            else
            {
                this.rtbTag.Text = "Identification tag failed!";
            }
        }
        /// <summary>
        /// 读取多卡处理事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timerReadMultiTag_Tick(object sender, EventArgs e)
        {
            byte idNum = 0;	// Max tag num is 200
            byte[] ids = new byte[12 * 200];	// The id length of each is 12, so 12 * 200 bytes is needed
            byte[] devNos = new byte[200];
            byte[] antennaNos = new byte[200];
            if (EPCSDKHelper.IdentifyUploadedMultiTags(_handle, out idNum, ids, devNos, antennaNos))
            {
                String strTmp = "";
                for (int j = 0; j < idNum; j++)
                {
                    byte[] bTmp = new byte[12];
                    Buffer.BlockCopy(ids, 12 * j, bTmp, 0, 12);
                    strTmp += string.Format("{0}    Device No:{1}  Antenna No: {2}\r\n", TextEncoder.ByteArrayToHexString(bTmp), devNos[j], antennaNos[j]);
                }
                this.rtbTag.Text = strTmp;
            }
            else
            {
                this.rtbTag.Text = "Identification tag failed!";
            }
        }

        private void btnReadSingleTag_Click(object sender, EventArgs e)
        {
            timerReadSingleTag.Enabled = true;
            timerReadSingleTag.Start();
        }

        private void btnStopReadSingleTag_Click(object sender, EventArgs e)
        {
            timerReadSingleTag.Stop();
        }

        private void btnReadMultiTag_Click(object sender, EventArgs e)
        {
            timerReadMultiTag.Enabled = true;
            timerReadMultiTag.Start();
        }

        private void btnStopReadMultiTag_Click(object sender, EventArgs e)
        {
            timerReadMultiTag.Stop();
        }

        private void btnRead_Click(object sender, EventArgs e)
        {
            byte[] data = new byte[12];
            if (EPCSDKHelper.ReadTag(_handle, 0x01, 0x02, 6, data, 0))	// 读EPC区地址02开始的6个字(12字节)内容
            {
                byte[] bTmp = new byte[12];
                Buffer.BlockCopy(data, 0, bTmp, 0, 12);
                this.txtTag.Text = TextEncoder.ByteArrayToHexString(bTmp);
            }
            else
            {
                this.txtTag.Text = "Reading tag content failed!";
            }

        }

        private void btnInitTag_Click(object sender, EventArgs e)
        {
            if (EPCSDKHelper.InitializeTag(_handle, 0))
            {
                toolStripStatusLabel1.Text = string.Format("Initialize the tag successfully!");
            }
            else
            {
                toolStripStatusLabel1.Text = string.Format("Initialize tag failed!");
            }
        }

        private void btnLock_Click(object sender, EventArgs e)
        {
            byte[] id = TextEncoder.HexStringToByteArray(this.textAccessPassword.Text);
            if (id.Length!= 4)
            {
                toolStripStatusLabel1.Text = string.Format("Please input 8-digit hexadecimal number access password!");
                return;
            }

            if (EPCSDKHelper.LockPassWordTag(_handle, id[0], id[1], id[2], id[3], 2, 0))  //02: LOCK EPC,03:LOCK ACCESS;
                toolStripStatusLabel1.Text = string.Format("Locked successfully!");
            else
                toolStripStatusLabel1.Text = string.Format("Lock failed!");         
        }

        private void btnKill_Click(object sender, EventArgs e)
        {
            if (EPCSDKHelper.KillTag(_handle, 0xff, 0xff, 0xff, 0xff, 0))// Lock All
            {
                toolStripStatusLabel1.Text = string.Format("Destroy Tag Success!");
            }
            else
            {
                toolStripStatusLabel1.Text = string.Format("Destroy tag failed!");
            }
        }

        private void btnFastWriteTag_Click(object sender, EventArgs e)
        {
            byte [] id = TextEncoder.HexStringToByteArray(this.txtTag.Text);
            if((id.Length%2)!=0)
            {
                toolStripStatusLabel1.Text = string.Format("Please enter 4,8,12,16,20,24-bit hexadecimal system!");
                return;
            }

            if (EPCSDKHelper.FastWriteTagID(_handle, id.Length, id, 0))
                toolStripStatusLabel1.Text = string.Format("Fast write success!");
            else
                toolStripStatusLabel1.Text = string.Format("Fast write failure!");
        }

        private void btnWrite_Click(object sender, EventArgs e)
        {
            byte[] id = TextEncoder.HexStringToByteArray(this.txtTag.Text);
            if (id.Length != 2)
            {
                toolStripStatusLabel1.Text = string.Format("Please enter 2 digits hexadecimal!");
                return;
            }

            if (EPCSDKHelper.WriteTagSingleWord(_handle, 0x01, 0x02, id[0], id[1], 0))
                toolStripStatusLabel1.Text = string.Format("Fast write success!");
            else
                toolStripStatusLabel1.Text = string.Format("Failed to write the label!");
        }

        //函数原型： BOOL WriteTagSingleWord(HANDLE hCom, BYTE memBank, BYTE address,BYTE data1, BYTE data2);
        //功能说明： 向标签写入1个字（2字节）的内容。（注：EPC区的地址0、1不可写入）
        //返回值： 成功时返回TRUE（1），失败时返回FALSE（0）
        //参数： ●——hCom： 串口句柄
        //●——memBank： 要写的区域。各值的意义如下：
        //0x00——保留区
        //0x01——EPC区
        //0x02——TID区
        //0x03——用户区
        //●——address： 要写区域中的地址，取值为范围0-7
        //（memBank为EPC区时， 0、 1不可取）。
        //●——data1： 要写入内容的第1个字节
        //●——data2： 要写入内容的第2个字节

        private void BtnIdentifyTag_Click(object sender, EventArgs e)
        {
            byte[] data = new byte[12];
            byte[] antennaNo = new byte[1];
            if (EPCSDKHelper.IdentifySingleTag(_handle, data, antennaNo, 0))	// 读卡内容
            {
                byte[] bTmp = new byte[12];
                
                Buffer.BlockCopy(data, 0, bTmp, 0, 12);
                this.txtTag.Text = TextEncoder.ByteArrayToHexString(bTmp);
            }
            else
            {
                this.txtTag.Text = "Identification tag failed!";
            }
        }

        private void txtTag_TextChanged(object sender, EventArgs e)
        {

        }

       
        private void textAccessPassword_TextChanged(object sender, EventArgs e)
        {
           
        }


        private void BtnUnlockTag_Click(object sender, EventArgs e)
        {
            byte[] id = TextEncoder.HexStringToByteArray(this.textAccessPassword.Text);
            if (id.Length != 4)
            {
                toolStripStatusLabel1.Text = string.Format("Please input 8-digit hexadecimal number access password!");
                return;
            }

            if (EPCSDKHelper.UnlockPassWordTag(_handle, id[0], id[1], id[2], id[3], 2, 0))  //02: LOCK EPC,03:LOCK ACCESS;
                toolStripStatusLabel1.Text = string.Format("Unlock success!");
            else
                toolStripStatusLabel1.Text = string.Format("Unlock failed!");   
        }

        private void WriteAccessPassword_Click(object sender, EventArgs e)
        {
            byte[] id = TextEncoder.HexStringToByteArray(this.txtTag.Text);
            if (id.Length != 4)
            {
                toolStripStatusLabel1.Text = string.Format("Please enter 8-digit hexadecimal number!");
                return;
            }

            if (EPCSDKHelper.WriteTagSingleWord(_handle, 0x00, 0x02, id[0], id[1], 0))
            {
                if (EPCSDKHelper.WriteTagSingleWord(_handle, 0x00, 0x03, id[2], id[3], 0))
                    toolStripStatusLabel1.Text = string.Format("Write access to password successfully!");
                else
                {
                    if (EPCSDKHelper.WriteTagSingleWord(_handle, 0x00, 0x03, id[2], id[3], 0))
                        toolStripStatusLabel1.Text = string.Format("Write access to password successfully!");
                    else
                        toolStripStatusLabel1.Text = string.Format("Write access to password failed!");
                }
            }
            else
                toolStripStatusLabel1.Text = string.Format("Write access to password failed!");
        }

        private void ReadAccessPassword_Click(object sender, EventArgs e)
        {
            byte[] data = new byte[4];
            if (EPCSDKHelper.ReadTag(_handle, 0x00, 0x02, 2, data, 0))	// 读保留区地址02开始的2个字(4字节)内容
            {
                byte[] bTmp = new byte[4];
                Buffer.BlockCopy(data, 0, bTmp, 0, 4);
                this.txtTag.Text = TextEncoder.ByteArrayToHexString(bTmp);
            }
            else
            {
                this.txtTag.Text = "Read Access Password Failed!";
            }
        }
        //函数原型： BOOL ReadTag(HANDLE hCom, BYTE memBank, BYTE address, BYTE length, BYTE* data);
        //返回值： 成功时返回TRUE（1），失败时返回FALSE（0）
        //参数 ●——hCom： 串口句柄
        //●——memBank： 要读的区域。各值的意义如下：
        //0x00——保留区
        //0x01——EPC区
        //0x02——TID区
        //0x03——用户区
        //●——address： 要读区域中的地址，取值为范围0-7。
        //●——length： 要读取的长度，取值范围是1到8（单位是Word，
        //1Word = 2Byte）。
        //●——data： 要写入内容的地址（输出参数）
     
    }
}
