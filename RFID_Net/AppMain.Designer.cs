﻿namespace RFID
{
    partial class AppMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnOpen = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.ReadAccessPassword = new System.Windows.Forms.Button();
            this.WriteAccessPassword = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.BtnUnlockTag = new System.Windows.Forms.Button();
            this.textAccessPassword = new System.Windows.Forms.TextBox();
            this.BtnIdentifyTag = new System.Windows.Forms.Button();
            this.btnRead = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTag = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnFastWriteTag = new System.Windows.Forms.Button();
            this.btnWrite = new System.Windows.Forms.Button();
            this.btnKill = new System.Windows.Forms.Button();
            this.btnLock = new System.Windows.Forms.Button();
            this.btnInitTag = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.rtbTag = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnStopReadMultiTag = new System.Windows.Forms.Button();
            this.btnReadMultiTag = new System.Windows.Forms.Button();
            this.btnStopReadSingleTag = new System.Windows.Forms.Button();
            this.btnReadSingleTag = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btnResumeReading = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnFirmwareVer = new System.Windows.Forms.Button();
            this.cbCom = new System.Windows.Forms.ComboBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.timerReadSingleTag = new System.Windows.Forms.Timer(this.components);
            this.timerReadMultiTag = new System.Windows.Forms.Timer(this.components);
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOpen
            // 
            this.btnOpen.Location = new System.Drawing.Point(96, 4);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(82, 25);
            this.btnOpen.TabIndex = 1;
            this.btnOpen.Text = "Connect";
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(188, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(82, 25);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(13, 35);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(460, 239);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.ReadAccessPassword);
            this.tabPage1.Controls.Add(this.WriteAccessPassword);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.BtnUnlockTag);
            this.tabPage1.Controls.Add(this.textAccessPassword);
            this.tabPage1.Controls.Add(this.BtnIdentifyTag);
            this.tabPage1.Controls.Add(this.btnRead);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.txtTag);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.btnFastWriteTag);
            this.tabPage1.Controls.Add(this.btnWrite);
            this.tabPage1.Controls.Add(this.btnKill);
            this.tabPage1.Controls.Add(this.btnLock);
            this.tabPage1.Controls.Add(this.btnInitTag);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(452, 213);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Write";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // ReadAccessPassword
            // 
            this.ReadAccessPassword.Location = new System.Drawing.Point(160, 138);
            this.ReadAccessPassword.Name = "ReadAccessPassword";
            this.ReadAccessPassword.Size = new System.Drawing.Size(129, 25);
            this.ReadAccessPassword.TabIndex = 13;
            this.ReadAccessPassword.Text = "Read access password";
            this.ReadAccessPassword.UseVisualStyleBackColor = true;
            this.ReadAccessPassword.Click += new System.EventHandler(this.ReadAccessPassword_Click);
            // 
            // WriteAccessPassword
            // 
            this.WriteAccessPassword.Location = new System.Drawing.Point(6, 138);
            this.WriteAccessPassword.Name = "WriteAccessPassword";
            this.WriteAccessPassword.Size = new System.Drawing.Size(130, 25);
            this.WriteAccessPassword.TabIndex = 12;
            this.WriteAccessPassword.Text = "Write access password";
            this.WriteAccessPassword.UseVisualStyleBackColor = true;
            this.WriteAccessPassword.Click += new System.EventHandler(this.WriteAccessPassword_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(254, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Access Password:";
            // 
            // BtnUnlockTag
            // 
            this.BtnUnlockTag.Location = new System.Drawing.Point(341, 73);
            this.BtnUnlockTag.Name = "BtnUnlockTag";
            this.BtnUnlockTag.Size = new System.Drawing.Size(105, 25);
            this.BtnUnlockTag.TabIndex = 10;
            this.BtnUnlockTag.Text = "Unlock the label";
            this.BtnUnlockTag.UseVisualStyleBackColor = true;
            this.BtnUnlockTag.Click += new System.EventHandler(this.BtnUnlockTag_Click);
            // 
            // textAccessPassword
            // 
            this.textAccessPassword.Location = new System.Drawing.Point(354, 43);
            this.textAccessPassword.Name = "textAccessPassword";
            this.textAccessPassword.Size = new System.Drawing.Size(92, 20);
            this.textAccessPassword.TabIndex = 9;
            this.textAccessPassword.TextChanged += new System.EventHandler(this.textAccessPassword_TextChanged);
            // 
            // BtnIdentifyTag
            // 
            this.BtnIdentifyTag.Location = new System.Drawing.Point(270, 107);
            this.BtnIdentifyTag.Name = "BtnIdentifyTag";
            this.BtnIdentifyTag.Size = new System.Drawing.Size(65, 25);
            this.BtnIdentifyTag.TabIndex = 8;
            this.BtnIdentifyTag.Text = "Read card";
            this.BtnIdentifyTag.UseVisualStyleBackColor = true;
            this.BtnIdentifyTag.Click += new System.EventHandler(this.BtnIdentifyTag_Click);
            // 
            // btnRead
            // 
            this.btnRead.Location = new System.Drawing.Point(178, 107);
            this.btnRead.Name = "btnRead";
            this.btnRead.Size = new System.Drawing.Size(86, 25);
            this.btnRead.TabIndex = 7;
            this.btnRead.Text = "Read the label";
            this.btnRead.UseVisualStyleBackColor = true;
            this.btnRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(189, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Note: The card number is hexadecimal";
            // 
            // txtTag
            // 
            this.txtTag.Location = new System.Drawing.Point(12, 59);
            this.txtTag.Name = "txtTag";
            this.txtTag.Size = new System.Drawing.Size(236, 20);
            this.txtTag.TabIndex = 5;
            this.txtTag.TextChanged += new System.EventHandler(this.txtTag_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "EPC:";
            // 
            // btnFastWriteTag
            // 
            this.btnFastWriteTag.Location = new System.Drawing.Point(97, 107);
            this.btnFastWriteTag.Name = "btnFastWriteTag";
            this.btnFastWriteTag.Size = new System.Drawing.Size(75, 25);
            this.btnFastWriteTag.TabIndex = 3;
            this.btnFastWriteTag.Text = "Write card";
            this.btnFastWriteTag.UseVisualStyleBackColor = true;
            this.btnFastWriteTag.Click += new System.EventHandler(this.btnFastWriteTag_Click);
            // 
            // btnWrite
            // 
            this.btnWrite.Location = new System.Drawing.Point(6, 107);
            this.btnWrite.Name = "btnWrite";
            this.btnWrite.Size = new System.Drawing.Size(75, 25);
            this.btnWrite.TabIndex = 3;
            this.btnWrite.Text = "Write card";
            this.btnWrite.UseVisualStyleBackColor = true;
            this.btnWrite.Click += new System.EventHandler(this.btnWrite_Click);
            // 
            // btnKill
            // 
            this.btnKill.Location = new System.Drawing.Point(345, 104);
            this.btnKill.Name = "btnKill";
            this.btnKill.Size = new System.Drawing.Size(95, 25);
            this.btnKill.TabIndex = 2;
            this.btnKill.Text = "Destroy the label";
            this.btnKill.UseVisualStyleBackColor = true;
            this.btnKill.Click += new System.EventHandler(this.btnKill_Click);
            // 
            // btnLock
            // 
            this.btnLock.Location = new System.Drawing.Point(345, 6);
            this.btnLock.Name = "btnLock";
            this.btnLock.Size = new System.Drawing.Size(101, 25);
            this.btnLock.TabIndex = 1;
            this.btnLock.Text = "Lock the label";
            this.btnLock.UseVisualStyleBackColor = true;
            this.btnLock.Click += new System.EventHandler(this.btnLock_Click);
            // 
            // btnInitTag
            // 
            this.btnInitTag.Location = new System.Drawing.Point(341, 135);
            this.btnInitTag.Name = "btnInitTag";
            this.btnInitTag.Size = new System.Drawing.Size(104, 25);
            this.btnInitTag.TabIndex = 0;
            this.btnInitTag.Text = "Initialize the label";
            this.btnInitTag.UseVisualStyleBackColor = true;
            this.btnInitTag.Click += new System.EventHandler(this.btnInitTag_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.rtbTag);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.btnStopReadMultiTag);
            this.tabPage2.Controls.Add(this.btnReadMultiTag);
            this.tabPage2.Controls.Add(this.btnStopReadSingleTag);
            this.tabPage2.Controls.Add(this.btnReadSingleTag);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(452, 213);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Read";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // rtbTag
            // 
            this.rtbTag.Location = new System.Drawing.Point(7, 68);
            this.rtbTag.Name = "rtbTag";
            this.rtbTag.Size = new System.Drawing.Size(427, 106);
            this.rtbTag.TabIndex = 2;
            this.rtbTag.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Label content:";
            // 
            // btnStopReadMultiTag
            // 
            this.btnStopReadMultiTag.Location = new System.Drawing.Point(292, 37);
            this.btnStopReadMultiTag.Name = "btnStopReadMultiTag";
            this.btnStopReadMultiTag.Size = new System.Drawing.Size(142, 25);
            this.btnStopReadMultiTag.TabIndex = 0;
            this.btnStopReadMultiTag.Text = "Stop reading more cards";
            this.btnStopReadMultiTag.UseVisualStyleBackColor = true;
            this.btnStopReadMultiTag.Click += new System.EventHandler(this.btnStopReadMultiTag_Click);
            // 
            // btnReadMultiTag
            // 
            this.btnReadMultiTag.Location = new System.Drawing.Point(292, 4);
            this.btnReadMultiTag.Name = "btnReadMultiTag";
            this.btnReadMultiTag.Size = new System.Drawing.Size(121, 25);
            this.btnReadMultiTag.TabIndex = 0;
            this.btnReadMultiTag.Text = "Read more cards";
            this.btnReadMultiTag.UseVisualStyleBackColor = true;
            this.btnReadMultiTag.Click += new System.EventHandler(this.btnReadMultiTag_Click);
            // 
            // btnStopReadSingleTag
            // 
            this.btnStopReadSingleTag.Location = new System.Drawing.Point(88, 35);
            this.btnStopReadSingleTag.Name = "btnStopReadSingleTag";
            this.btnStopReadSingleTag.Size = new System.Drawing.Size(164, 25);
            this.btnStopReadSingleTag.TabIndex = 0;
            this.btnStopReadSingleTag.Text = "Stop reading a single card";
            this.btnStopReadSingleTag.UseVisualStyleBackColor = true;
            this.btnStopReadSingleTag.Click += new System.EventHandler(this.btnStopReadSingleTag_Click);
            // 
            // btnReadSingleTag
            // 
            this.btnReadSingleTag.Location = new System.Drawing.Point(88, 4);
            this.btnReadSingleTag.Name = "btnReadSingleTag";
            this.btnReadSingleTag.Size = new System.Drawing.Size(113, 25);
            this.btnReadSingleTag.TabIndex = 0;
            this.btnReadSingleTag.Text = "Open a single card";
            this.btnReadSingleTag.UseVisualStyleBackColor = true;
            this.btnReadSingleTag.Click += new System.EventHandler(this.btnReadSingleTag_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.btnResumeReading);
            this.tabPage3.Controls.Add(this.btnStop);
            this.tabPage3.Controls.Add(this.btnFirmwareVer);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(452, 213);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Basic ops";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // btnResumeReading
            // 
            this.btnResumeReading.Location = new System.Drawing.Point(7, 72);
            this.btnResumeReading.Name = "btnResumeReading";
            this.btnResumeReading.Size = new System.Drawing.Size(136, 25);
            this.btnResumeReading.TabIndex = 2;
            this.btnResumeReading.Text = "Reset the reading head";
            this.btnResumeReading.UseVisualStyleBackColor = true;
            this.btnResumeReading.Click += new System.EventHandler(this.btnResumeReading_Click);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(7, 39);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(120, 25);
            this.btnStop.TabIndex = 1;
            this.btnStop.Text = "Stop reading the tag";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnFirmwareVer
            // 
            this.btnFirmwareVer.Location = new System.Drawing.Point(6, 7);
            this.btnFirmwareVer.Name = "btnFirmwareVer";
            this.btnFirmwareVer.Size = new System.Drawing.Size(175, 25);
            this.btnFirmwareVer.TabIndex = 0;
            this.btnFirmwareVer.Text = "Read Firmware version";
            this.btnFirmwareVer.UseVisualStyleBackColor = true;
            this.btnFirmwareVer.Click += new System.EventHandler(this.btnFirmwareVer_Click);
            // 
            // cbCom
            // 
            this.cbCom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCom.FormattingEnabled = true;
            this.cbCom.Items.AddRange(new object[] {
            "COM1",
            "COM2",
            "COM3",
            "COM4",
            "COM5",
            "COM6",
            "COM7",
            "COM8",
            "COM9",
            "COM10"});
            this.cbCom.Location = new System.Drawing.Point(13, 5);
            this.cbCom.Name = "cbCom";
            this.cbCom.Size = new System.Drawing.Size(73, 21);
            this.cbCom.TabIndex = 3;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 288);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(579, 22);
            this.statusStrip1.TabIndex = 5;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(107, 17);
            this.toolStripStatusLabel1.Text = "The device is ready";
            // 
            // timerReadSingleTag
            // 
            this.timerReadSingleTag.Interval = 500;
            this.timerReadSingleTag.Tick += new System.EventHandler(this.timerReadSingleTag_Tick);
            // 
            // timerReadMultiTag
            // 
            this.timerReadMultiTag.Tick += new System.EventHandler(this.timerReadMultiTag_Tick);
            // 
            // AppMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(579, 310);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.cbCom);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnOpen);
            this.Name = "AppMain";
            this.Text = "RFID read and write DEMO C # version";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ComboBox cbCom;
        private System.Windows.Forms.RichTextBox rtbTag;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnReadSingleTag;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btnFirmwareVer;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnResumeReading;
        private System.Windows.Forms.Button btnStopReadMultiTag;
        private System.Windows.Forms.Button btnReadMultiTag;
        private System.Windows.Forms.Button btnStopReadSingleTag;
        private System.Windows.Forms.Timer timerReadSingleTag;
        private System.Windows.Forms.Timer timerReadMultiTag;
        private System.Windows.Forms.Button btnInitTag;
        private System.Windows.Forms.Button btnLock;
        private System.Windows.Forms.Button btnKill;
        private System.Windows.Forms.TextBox txtTag;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnFastWriteTag;
        private System.Windows.Forms.Button btnWrite;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnRead;
        private System.Windows.Forms.Button BtnIdentifyTag;
        private System.Windows.Forms.Button BtnUnlockTag;
        private System.Windows.Forms.TextBox textAccessPassword;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button WriteAccessPassword;
        private System.Windows.Forms.Button ReadAccessPassword;
    }
}

